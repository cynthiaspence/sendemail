const nodemailer = require('nodemailer')
const path = require('path')
const fs = require('fs')

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'your.email@company.com',
    pass: 'YourGeneratedPasscode',
  },
})

const htmlDirPath = 'PATH_TO/platform/packages/tasks/src/email/tmp'

const fileNames = [
  '/*GENERATED HTML EMAIL FILE NAME*/.html',
]

for (const fileName of fileNames) {
  const filePath = path.resolve(htmlDirPath, fileName)

  const emailHtml = fs.readFileSync(filePath, { encoding: 'utf-8' })

  transporter.sendMail(
    {
      from: 'your.email@company.com',
      to: 'your.email@company.com',
      subject: `Test Email: ${filePath}`,
      html: emailHtml
    },
    (err, info) => {
      if (err) {
        console.log(err)
      } else {
        console.log(info)
      }
    }
  )
}